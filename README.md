# 2 port isolated USB hub

Target of the project is to create a barebone isolated USB hub that can be mounted on devices that consitite a final product.  
Isolated USB hubs are much more resistant to electromagnetic interference than USB hubs that do not use isolation.  
Currently, in the market isolated USB hubs are not easily found or are expensive due to the testings and simulations done to prove their immunity.  

Pros:   
-Barebone option available    
-Low cost  
-Small form factor  
-Isolation on upstream.  
-Can operate with power from the upstream using the optional short for VCC/GND from the upstream.  
-Optional short/jumper resistors are placed on Hole mounts and USB ports if they are needed.  

Cons:  
-Not undergone official EMI tests.  
-EMI tests that conducted are not (yet) described.   

The current USB hub(s) designed did not undergo official EMI tests and does not protect against electrostatic discharge.

![isolated usb hub](https://i.stack.imgur.com/5hXoI.png)
